#!/bin/bash

composer install --no-scripts --no-interaction 
# npm install
composer dumpautoload
ln -f -s .env.pipelines .env

php  artisan cache:clear
php artisan migrate

# npm run prod